#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <netinet/sctp.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <pthread.h>
#define SIZE 100
#define NODES 45
#define QSIZE 45
#define BUFFER_SIZE 128
#define true 1
#define false 0
typedef int bool;

// Global variables
char config [SIZE];

struct Neighbor 
{
	int	nID;
	char hostname [SIZE];
	int	port;
};

struct Node  //structure of node 
{
	int	nID;
	char hostname [SIZE];
	int	port;	
	struct Neighbor neighbors [NODES];
	int neighCount;
};

void emptyNeighbor (struct Neighbor *);
void getNeighbor (int, struct Neighbor *);
void initializeNeighbor (char *, struct Neighbor *);
void setNeighbor (int, char *, int, struct Neighbor *);	
bool isPresent (struct Neighbor, struct Neighbor *, int);

void emptyNode (struct Node *);
void getNode (int, struct Node *);
void setNode (int, char *, int, struct Neighbor *, int, struct Node *);
void messageToSend (char *, struct Node *);

void * hostServer (void *);
void QInsert (struct Neighbor *, int *, struct Neighbor);
struct Neighbor QDelete (struct Neighbor *, int *);
bool QEmpty (struct Neighbor *, int, int);

void emptyNeighbor (struct Neighbor * node)
{
	node->nID = -1;
	node->port = -1;
	strcpy (node->hostname, "");
}

void getNeighbor (int nID, struct Neighbor * node) 
{
	FILE * fPtr;
	char buffer [SIZE];
	char nodeID [SIZE];
	char hostname [SIZE];
	char port [SIZE];
	int neighs [SIZE];
	int nCount;
	
	fPtr = NULL;
	fPtr = fopen (config, "r");
	
	while (1)
	{
		strcpy (nodeID, "");
		if (fscanf (fPtr, "%s", nodeID) == EOF)
			break;
		
		strcpy (hostname, "");
		fscanf (fPtr, "%s", hostname) ;
		
		strcpy (port, "");
		fscanf (fPtr, "%s", port) ;
		
		nCount = -1;
		fscanf (fPtr, "%s", buffer) ;
		while (strcmp (buffer, "$")) 
		{
			neighs [++nCount] = atoi (buffer);
			fscanf (fPtr, "%s", buffer);
		}
		
		if (atoi (nodeID) == nID) 
		{
			node->port = atoi (port);
			strcpy (node->hostname, hostname);
			node->nID = nID;
			
			fclose (fPtr);
			return;
		}
	}
	fclose (fPtr);
}

void initializeNeighbor (char * buffer, struct Neighbor * node)
{
	char hostname [SIZE];
	int	port;
	int nID;
	
	char * str = NULL;
	str = strtok (buffer, ":");
	nID = atoi (str);
	
	str = strtok (NULL, ":");
	strcpy (hostname, str);
	
	str = strtok (NULL, ":");
	port = atoi (str);
	
	node->nID = nID;
	node->port = port;
	strcpy (node->hostname, hostname);
}

void setNeighbor (int nID, char * hostname, int port, struct Neighbor * node) 
{
	node->nID = nID;
	node->port = port;
	strcpy (node->hostname, hostname);
}

bool isPresent (struct Neighbor node, struct Neighbor * knownNodes, int count) 
{
	int i;
	for (i = 0; i < count; i++) {
		if (knownNodes[i].nID == node.nID)
			return true;
	}
	return false;
}

void emptyNode (struct Node * node)
{
	node->nID = -1;
	node->port = -1;
	strcpy (node->hostname, "");
	node->neighCount = 0;
}

void getNode (int nID, struct Node * node)
{
	FILE * fPtr;
	struct Neighbor temp;
	char buffer [SIZE];
	char nodeID [SIZE];
	char hostname [SIZE];
	char port [SIZE];
	int neighs [SIZE];
	int nCount;
	int i;
	
	fPtr = NULL;
	fPtr = fopen (config, "r");
	
	while (1)
	{
		strcpy (nodeID, "");
		if (fscanf (fPtr, "%s", nodeID) == EOF)
			break;
		
		strcpy (hostname, "");
		fscanf (fPtr, "%s", hostname) ;
		
		strcpy (port, "");
		fscanf (fPtr, "%s", port) ;
		
		nCount = 0;
		fscanf (fPtr, "%s", buffer) ;
		while (strcmp (buffer, "$")) 
		{
			neighs [nCount++] = atoi (buffer);
			fscanf (fPtr, "%s", buffer);
		}
		
		if (atoi (nodeID) == nID) 
		{
			node->port = atoi (port);
			strcpy (node->hostname, hostname);
			node->nID = nID;
			node->neighCount = nCount;
			
			for (i = 0; i < nCount; i++)
			{
				getNeighbor (neighs[i], &temp);
				node->neighbors[i] = temp;
			}
			fclose (fPtr);
			return;
		}
	}
	fclose (fPtr);
}

void setNode (int nID, char * hostname, int port, struct Neighbor * neighs, int nCount, struct Node * node)
{
	int i;
	
	node->nID = nID;
	strcpy (node->hostname, hostname);
	node->port = port;
	node->neighCount = nCount;
	
	for (i = 0; i < nCount; i++)
		node->neighbors[i] = neighs[i];
}

void messageToSend (char * buffer, struct Node * node)
{
	char temp [BUFFER_SIZE];
	int i;
	
	strcpy (buffer, "");
	for (i = 0; i < node->neighCount; i++) 
	{
		sprintf (temp, "%d:%s:%d$", node->neighbors[i].nID, node->neighbors[i].hostname, node->neighbors[i].port);
		strcat (buffer, temp);
	}
}

void * hostServer (void * threadArg) 
{
	char msgToSend [BUFFER_SIZE];
	char buffer [BUFFER_SIZE + 1];
	int listenSock, connSock, ret, in, flags;
	struct Node * node;
	struct sockaddr_in servaddr;
	struct sctp_initmsg	initmsg;
	struct sctp_event_subscribe	events;
	
	node = (struct Node *) threadArg;
	
	memset (msgToSend, 0, BUFFER_SIZE);
	messageToSend (msgToSend, node);
	
	listenSock = socket (AF_INET, SOCK_STREAM, IPPROTO_SCTP); 

	bzero ((void *) &servaddr, sizeof (servaddr));
	servaddr.sin_family = AF_INET;
	servaddr.sin_addr.s_addr = htonl (INADDR_ANY);
	servaddr.sin_port = htons (node->port);

	ret = bind (listenSock, (struct sockaddr *) &servaddr, (long) sizeof (servaddr));
	
	/* Specify that a maximum of 5 streams will be available per socket */
	memset (&initmsg, 0, sizeof (initmsg));
	initmsg.sinit_num_ostreams = 5;
	initmsg.sinit_max_instreams = 5;
	initmsg.sinit_max_attempts = 4;
	
	ret = setsockopt (listenSock, IPPROTO_SCTP, SCTP_INITMSG, &initmsg, sizeof (initmsg));
	listen (listenSock, 5);
	
	while (1) 
	{
		/*Awaiting a new connection*/
		memset (buffer, 0, BUFFER_SIZE + 1);
		connSock = accept (listenSock, (struct sockaddr *) NULL, (socklen_t *) NULL);
		
		if (connSock == -1) 
		{
			perror ("accept ()");
			exit (-1);
		}
		
		in = sctp_recvmsg (connSock, (void *) buffer, sizeof (buffer), (struct sockaddr *) NULL, 0, 0, 0);
		if (!strcmp (buffer, "WHAT_ARE_YOUR_NEIGHBORS")) {
			ret = sctp_sendmsg (connSock, (void *) msgToSend, (size_t) strlen (msgToSend), NULL, 0, 0, 0, 0, 0, 0);
		}
		close (connSock);
	}
}

void QInsert (struct Neighbor * dQueue, int * qRear, struct Neighbor node)
{
	if (*qRear == QSIZE-1)
	{
		printf ("Queue Overflow\n");
		exit (-1);
	} 
	else
		dQueue [++(*qRear)] = node;
}

struct Neighbor QDelete (struct Neighbor * dQueue, int * qFront)
{
	return dQueue [(*qFront)++];
}

bool QEmpty (struct Neighbor * dQueue, int qFront, int qRear)
{
	if (qRear < qFront)
		return true;
	else
		return false;
}

int main (int argc, char * argv[]) 
{
	struct Node	node;
	int	curNodeID;
	struct Neighbor knownNodes [QSIZE];
	struct Neighbor dQueue [QSIZE];
	struct Neighbor tNeighbor;
	struct Neighbor nNode;
	int qFront, qRear;
	int	knownNodeCount;
	FILE * fPtr;
	int	threadID;
	pthread_t serverInstance;
	
	// A SCTP Client Variables
	int connSock, in, ret, flags, i;
	struct hostent * pHostInfo;
	struct sockaddr_in servaddr;
	struct sctp_status status;
	long nHostAddress;
	struct sctp_sndrcvinfo sndrcvinfo;
	
	char buffer [BUFFER_SIZE + 1];
	char neighbors [BUFFER_SIZE];
	char hostname [BUFFER_SIZE];
	char infoFromNeighbor [10][SIZE];
	char * str;
	int nInfoCount;
	
	knownNodeCount = 0;
	nInfoCount = 0;
	qFront = 0;
	qRear = -1;
	str = NULL;
	
	strcpy (config, "config");
	
	if (argc < 2) 
	{
		printf ("please enter a Node ID  and Config File Name\n\n");
		return -1;
	}
	
	if (argc == 3)
		strcpy (config, argv[2]);

	curNodeID = atoi (argv[1]);
	getNode (curNodeID, &node);

	strcpy (neighbors, "");
	messageToSend (neighbors, &node);
	
	sprintf (buffer, "node%d", node.nID);	
	fPtr = fopen (buffer, "w");
	
	fprintf (fPtr, "Node - %d\nHostname - %s\nPort Number - %d\nNumber of neighbors: %d\n", node.nID, node.hostname, node.port, node.neighCount);
	for (i = 0; i < node.neighCount; i++) 
		fprintf (fPtr, "%d\t", node.neighbors[i].nID);
	fprintf (fPtr, "\n\n");
	
	threadID = pthread_create (&serverInstance, NULL, hostServer, (void *) &node);
	if (threadID) 
	{
		fprintf (fPtr, "Error in thread creation \n");
		fclose (fPtr);
		return 1;
	}
	
	// Wait for server to start
	sleep (1);
	
	setNeighbor (node.nID, node.hostname, node.port, &tNeighbor);
	knownNodes [knownNodeCount++] = tNeighbor;
	
	for (i = 0; i < node.neighCount; i++) 
	{
		QInsert (dQueue, &qRear, node.neighbors[i]);
		knownNodes [knownNodeCount++] = node.neighbors[i];
	}
	
	fprintf (fPtr, "Initial Knowledge: ");
	for (i = 0; i < knownNodeCount; i++)
		fprintf (fPtr, "%d\t", knownNodes[i].nID);
	fprintf (fPtr, "\n");
	
	while (!QEmpty (dQueue, qFront, qRear)) 
	{
		nNode = QDelete (dQueue, &qFront);
		connSock = socket (AF_INET, SOCK_STREAM, IPPROTO_SCTP);
		if (connSock == -1) 
		{
			perror ("socket ()");
			fclose (fPtr);
			return -1;
		}

		// fprintf (fPtr, "Node: %d, HostName: %s, Port: %d\n", nNode.nID, nNode.hostname, nNode.port);
		bzero ((void *) &servaddr, sizeof (servaddr));
		pHostInfo = gethostbyname (nNode.hostname);
		memcpy (&nHostAddress, pHostInfo->h_addr, pHostInfo->h_length);
		
		servaddr.sin_family = AF_INET;
		servaddr.sin_port = htons (nNode.port);
		servaddr.sin_addr.s_addr = nHostAddress;
		
		if (connect (connSock, (struct sockaddr *) &servaddr, sizeof(servaddr)) < 0)
		{
			perror ("connect ()");
			return -1;
		}

		strcpy (buffer, "WHAT_ARE_YOUR_NEIGHBORS");
		ret = sctp_sendmsg (connSock, (void *) buffer, (size_t) strlen (buffer), NULL, 0, 0, 0, 0, 0, 0 );
		fprintf (fPtr, "Contacted %d for information\n", nNode.nID);
		memset (buffer, 0, BUFFER_SIZE + 1);
		in = sctp_recvmsg (connSock, (void *) buffer, sizeof (buffer), (struct sockaddr *) NULL, 0, &sndrcvinfo, &flags);
		
		while (!strcmp (buffer, "WHAT_ARE_YOUR_NEIGHBORS")) 
		{
			sctp_sendmsg (connSock, (void *) neighbors, (size_t) strlen (neighbors), NULL, 0, 0, 0, 0, 0, 0 );
			memset (buffer, 0, BUFFER_SIZE + 1);
			sctp_recvmsg (connSock, (void *) buffer, sizeof (buffer), (struct sockaddr *) NULL, 0, &sndrcvinfo, &flags);
		}
		fprintf (fPtr, "Response received for the request made\n");
		
		nInfoCount = 0;
		str = strtok (buffer, "$");
		while (str != NULL) 
		{
			strcpy (infoFromNeighbor [nInfoCount++], str);
			str = strtok (NULL, "$");
		}
		
		for (i = 0; i < nInfoCount; i++) 
		{
			initializeNeighbor (infoFromNeighbor[i], &tNeighbor);
			fprintf (fPtr, "Discovered %d. Checking in current knowledge for repetition.\n", tNeighbor.nID);
			
			if (!isPresent (tNeighbor, knownNodes, knownNodeCount)) 
			{
				fprintf (fPtr, "Not repeated. Entering %d in queue\n", tNeighbor.nID);
				QInsert (dQueue, &qRear, tNeighbor);
				knownNodes [knownNodeCount++] = tNeighbor;
			}
		}
		close (connSock);
		
		fprintf (fPtr, "Current Knowledge: ");
		for (i = 0; i < knownNodeCount; i++) 
			fprintf (fPtr, "%d\t", knownNodes[i].nID);
		fprintf (fPtr, "\n");
	}
	
	fprintf (fPtr, "Completed Node Discovery\n", node.nID);
	fprintf (fPtr, "Final Knowledge: ", node.nID);
	for (i = 0; i < knownNodeCount; i++) 
		fprintf (fPtr, "%d\t", knownNodes[i].nID);
	fprintf (fPtr, "\n");
	
	printf ("Node %d: Completed Node Discovery\n", node.nID);
	sleep (10);

	return 0;
}
